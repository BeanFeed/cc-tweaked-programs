local margin = 20
local xOffset = 0
local zOffset = 0

local emptyTrackColor = colors.gray
local occupiedTrackColor = colors.white
local offSignalColor = colors.red
local onSignalColor = colors.green

local function contains(list, item) 
  for i, v in ipairs(list) do
    if v == item then return true end

  end

  return false
end

local function printCorner(str) 
  local x,y = term.getCursorPos()
  term.setCursorPos(1,2)
  term.setBackgroundColor(colors.black)
  print(str)
  --term.setCursorPos(x,y)
end

local function tableLength(T)
  local count = 0
  if T == nil then return 0 end
  for _ in pairs(T) do count = count + 1 end
  return count
end

--nextPoint should be the signal to draw the line to
--signal should be the the point you are drawing lines from



local args = {...}
local devMode = false
if contains(args, "-d") then
  devMode = true
end


local monitorSide
local serverID
local m

if devMode == false then 
  monitorSide = "right"
  serverID = 6;
  m = peripheral.wrap(monitorSide)
  m.setTextScale(0.5)
  term.redirect(m)
  rednet.open("back")
end

term.setBackgroundColor(colors.black)
term.clear()
term.setCursorPos(1,1)
--Monitor Width and Length
local mW, mL = term.getSize()


--Getting The Track Layout Data
local packet = {
    ["type"] = "railwayDisplayRequestAllData"
}
local type = ""
local data = {
  type = "railwayDisplayAllData",
  signals = {
    W1 = {
      connectsTo = {
        W2 = {
          trackSpine = {},
        },
      },
      x = 562,
      z = -551,
      initialState = false
    },
    N1 = {
      connectsTo = {
        S1 = {
          trackSpine = {},
        },
      },
      x = 568,
      z = -555,
      initialState = false
    },
    E1 = {
      connectsTo = {
        W1 = {
          trackSpine = {},
        },
      },
      x = 561,
      z = -547,
      initialState = true
    },
    W2 = {
      connectsTo = {
        E2 = {
          trackSpine = {},
        },
      },
      x = 581,
      z = -551,
      initialState = false
    },
    S1 = {
      connectsTo = {
        W2 = {
          trackSpine = {
            {
              x = 573,
              z = -551
            }
          },
        },
      },
      x = 573,
      z = -554,
      initialState = false
    },
    E2 = {
      connectsTo = {
        E1 = {
          trackSpine = {},
        },
        N1 = {
          trackSpine = {
            {
              x = 568,
              z = -547
            }
          },
        },
      },
      x = 580,
      z = -547,
      initialState = false
    },
  },
}
local function getAllPoints(signal, nextPoint, nextPointKey)
  --term.setCursorPos(1,2)
  --term.setBackgroundColor(colors.black)
  --print(textutils.serialise(nextPoint))
local points = {}
if(signal == nil) then error("get all points Signal is nil", 1) end
local toSigs = nextPoint.trackSpine
table.insert(points, {x = signal.x, z = signal.z})
--local track = 2
for k,v in pairs(toSigs) do
  
  table.insert(points, {["x"] = v.x, ["z"] = v.z})
  --track = track + 1
end
table.insert(points, {["x"] = data.signals[nextPointKey].x, ["z"] = data.signals[nextPointKey].z})

return points
end

while devMode == false and type ~= "railwayDisplayAllData" do
    rednet.send(serverID, packet, "railwayDisplay")
    local id, m = rednet.receive("railwayDisplay")
    type = m["type"]
    data = m
    os.sleep(1)
end

local function drawTrack() 
  local allNil = true
  if data == nil or data["signals"] == nil then return nil end

  for k, v in pairs(data["signals"]) do
    local colorToUse = emptyTrackColor

    local leadingPoints = v.connectsTo
      for k2, v2 in pairs(leadingPoints) do
        if(tableLength(leadingPoints[k2].trackSpine) == 0) then
          
          paintutils.drawLine(v.x,v.z,data.signals[k2].x, data.signals[k2].z, colorToUse)
        else
          allNil = false
          local points = getAllPoints(v, v2, k2)
              local prev = {["x"] = v.x, ["z"] = v.z}
              --printCorner(textutils.serialise(points))
              for pK, pV in pairs(points) do
                
                  paintutils.drawLine(prev.x, prev.z, pV.x, pV.z, colorToUse)
                  prev = pV
              end
        end
      end

  end
  term.setCursorPos(1,2)
  term.setBackgroundColor(colors.black)
  --print(allNil)
end

local function drawSignals()
  for k,v in pairs(data["signals"]) do
    local colorToUse = offSignalColor
    if(v.initialState == true) then colorToUse = onSignalColor end
    paintutils.drawPixel(v.x, v.z, colorToUse)
    --print("h")
  end
end

local function round(num)
    return math.floor(num + 0.5)
end


local smallestX = nil
local largestX = nil
local smallestZ = nil
local largestZ = nil
for k,v in pairs(data["signals"]) do
    if(smallestX == nil or v.x < smallestX) then 
        smallestX = v.x
    elseif(largestX == nil or v.x > largestX) then 
        largestX = v.x 
    end
    if(smallestZ == nil or v.z < smallestZ) then 
        smallestZ = v.z
    elseif(largestZ == nil or v.z > largestZ) then 
        largestZ = v.z
    end
    local leadingPoints = v.connectsTo
    for k2, v2 in pairs(leadingPoints) do
      local points = getAllPoints(v,v2,k2)
      --printCorner(textutils.serialise(points))
      for k3, v3 in pairs(points) do
        --printCorner(v3.x)
        if(smallestX == nil or v3.x < smallestX) then 
          smallestX = v3.x
        elseif(largestX == nil or v3.x > largestX) then 
            largestX = v3.x 
        end
        if(smallestZ == nil or v3.z < smallestZ) then 
          smallestZ = v3.z
        elseif(largestZ == nil or v3.z > largestZ) then 
            largestZ = v3.z
        end  
        
      end
    end
end


--print("Z Scale: " .. zScale)
--textutils.slowPrint(textutils.serialise(data),10)
for k,v in pairs(data["signals"]) do
    v.x = round(((((v.x - smallestX) * (mW - (1 + margin))) / (largestX - smallestX)) + 1 + margin) - margin / 2) + xOffset
    v.z = round(((((v.z - smallestZ) * (mL - (1 + margin))) / (largestZ - smallestZ)) + 1 + margin) - margin / 2) + zOffset
    for k2, v2 in pairs(v.connectsTo) do
      for k3, v3 in pairs(v2.trackSpine) do
        v3.x = round(((((v3.x - smallestX) * (mW - (1 + margin))) / (largestX - smallestX)) + 1 + margin) - margin / 2) + xOffset
        v3.z = round(((((v3.z - smallestZ) * (mL - (1 + margin))) / (largestZ - smallestZ)) + 1 + margin) - margin / 2) + zOffset
      end
    end
    --print("X: " .. v.x)
    --paintutils.drawPixel(v.x, v.z, colors.red)
end

drawTrack()
os.sleep(0.1)
drawSignals()

while true do
  local id, vals = rednet.receive("railwayDisplay")
  if (id == serverID and vals["type"] == "railwaySignalState") then 
    local signal = data.signals[vals["signalID"]]
    local colorToUse = offSignalColor
    --[[
    if(vals.signalDisplayState == true) then colorToUse = onSignalColor end
    local leadingPoints = signal.connectsTo
    for k, v in pairs(leadingPoints) do
      if(tableLength(leadingPoints[k].trackSpine) == 0) then
      paintutils.drawLine(v.x,v.z,data.signals[k].x, data.signals[k].z, colorToUse)
      else
        allNil = false
        local points = getAllPoints(signal, v, k)
            local prev = {["x"] = v.x, ["z"] = v.z}
            --printCorner(textutils.serialise(points))
            for pK, pV in pairs(points) do
              
                paintutils.drawLine(prev.x, prev.z, pV.x, pV.z, colorToUse)
                prev = pV
            end
      end
    end
    ]]
    if(vals.signalDisplayState == true) then colorToUse = onSignalColor end
    paintutils.drawPixel(signal.x, signal.z, colorToUse)

  end
  os.sleep(0)
end
